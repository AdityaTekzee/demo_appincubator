//
//  SongSearchCell.swift
//  Demo
//
//  Created by mac on 15/06/21.
//
import UIKit

class SongSearchCell : UICollectionViewCell {
    
    @IBOutlet weak var artistUserImage: UIImageView!
    @IBOutlet weak var lblNameArtist: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.layer.borderWidth = 0.5
        contentView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0)
        contentView.layer.cornerRadius =  8
        contentView.clipsToBounds = true
        
        
    }
    func prepareLayout(objDashboard:SongSearchModel?) {
        lblNameArtist.text = objDashboard?.Title
        let getImageUrl = objDashboard?.Poster
        let fileUrl = URL(string: getImageUrl!)
        downloadImage(url: fileUrl!, completion: {
            (
                getimageViee,nil
            ) in
            DispatchQueue.main.async {
                self.artistUserImage.image = getimageViee
            }
        })
        
    }
    func downloadImage(url: URL, completion: @escaping (_ image: UIImage?, _ error: Error? ) -> Void) {
        let imageCache = NSCache<NSString, UIImage>()
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage, nil)
        } else {
            ImageLoader.downloadData(url: url) { data, response, error in
                if let error = error {
                    completion(nil, error)
                    
                } else if let data = data, let image = UIImage(data: data) {
                    imageCache.setObject(image, forKey: url.absoluteString as NSString)
                    completion(image, nil)
                } else {
                    completion(nil, NSError.generalParsingError(domain: url.absoluteString))
                }
            }
        }
    }
}
