//
//  ViewController.swift
//  Demo
//
//  Created by mac on 15/06/21.
//

import UIKit

class SongSearchViewController: UIViewController {
    
    @IBOutlet weak var  collectionView:UICollectionView!
    @IBOutlet weak var  viewSearch:UIView!
    private var edgeInsetPadding = 10
    var page: Int = 0
    var songSearchVMObj = SongSearchViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSearch.layer.cornerRadius =  16
        viewSearch.clipsToBounds = true
        prepareViewModelObserver()
        page = 1
        Pageination.peginationOfSongSearch = String(page)
        songSearchVMObj.requestSongData = []
        songSearchVMObj.requestSongDataPage = []
        fetchSongSearchList()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if(self.collectionView.contentOffset.y >= (self.collectionView.contentSize.height - self.collectionView.bounds.size.height)) {
            if !Pageination.isPageRefreshing {
                Pageination.isPageRefreshing = true
                print(page)
                page = page + 1
                Pageination.peginationOfSongSearch = String(page)
                fetchSongSearchList()
            }
        }
    }

    
    
}
// MARK: Extension
extension SongSearchViewController : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return songSearchVMObj.requestSongData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let nearCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SongSearchCell", for: indexPath) as! SongSearchCell
        if songSearchVMObj.requestSongData?.count ?? 0>0{
            nearCell.prepareLayout(objDashboard: songSearchVMObj.requestSongData?[indexPath.row])
            
        }
        return nearCell
    }
    
   
}
extension SongSearchViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((SCREEN_WIDTH-20)/2)-20, height:((SCREEN_WIDTH-20)/2)-20)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        edgeInsetPadding = Int(inset.left+inset.right)
        return inset
    }
    
}
extension SongSearchViewController {
    
    func fetchSongSearchList() {
        songSearchVMObj.fetchSongList()
    }
    
    func prepareViewModelObserver() {
        self.songSearchVMObj.songDidChanges = { (finished, error) in
            if !error {
                self.reloadTableView()
            }
        }
    }
    
    func reloadTableView() {
        self.collectionView.reloadData()
    }
}
