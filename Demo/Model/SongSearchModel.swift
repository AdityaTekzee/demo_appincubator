//
//  SongSearchModel.swift
//  Demo
//
//  Created by mac on 15/06/21.
//

import Foundation
import UIKit

class SongSearchModel: NSObject {
    
    let Title: String?
    let Year: String?
    let imdbID: String?
    let TypeSong: String?
    let Poster: String?
    
   
        init(dictInfo:[String:Any]) {
        
        Title = dictInfo["Title"] as? String
        Year = dictInfo["Year"] as? String
        imdbID = dictInfo["imdbID"] as? String
        TypeSong = dictInfo["Type"] as? String
        Poster = dictInfo["Poster"] as? String
    }
    
   
    
}
