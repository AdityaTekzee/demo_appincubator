
//
//  SongSearchAPiService.swift
//  Demo
//
//  Created by mac on 16/06/21.
//

import Foundation

class SongSearchAPIService: NSObject, Requestable {
    
    static let instance = SongSearchAPIService()



func fetchSongsList(callback: @escaping Handler) {
    
    request(method: .get, url: Domain.baseUrl() + APIEndpoint.moviesSearch + Pageination.peginationOfSongSearch) { (result) in
        
       callback(result)
    }
    
}
}
