//
//  SongSearchViewModel.swift
//  Demo
//
//  Created by mac on 16/06/21.
//

import Foundation
import UIKit

protocol SongSearchViewModelProtocol {
    
    var songDidChanges: ((Bool, Bool) -> Void)? { get set }
    func fetchSongList()
}
class SongSearchViewModel: SongSearchViewModelProtocol {
    
    

    // var requestFriend: [SongSearchModel]?
    //MARK: - Internal Properties
    var songDidChanges: ((Bool, Bool) -> Void)?
    var requestSongData: [SongSearchModel]? {
        didSet {
            self.songDidChanges!(true, false)
        }
    }
    var requestSongDataPage : [SongSearchModel]?
    func fetchSongList() {
        SongSearchAPIService.instance.fetchSongsList { result in
            switch result {
            case .success(let data):
                do {
                    let json = try JSONSerialization.jsonObject(with: data) as! Dictionary<String, AnyObject>
                    let arrMenu = (json as [String:Any])["Search"] as! [[String:Any]]
                    var arrDashboard = [SongSearchModel]()
                    for obj in arrMenu {
                        let objDashboard = SongSearchModel(dictInfo: obj)
                        arrDashboard.append(objDashboard)
                    }
                    if arrDashboard.count == 0{
                        Pageination.isPageRefreshing = true
                    } else {
                        self.requestSongDataPage = arrDashboard
                        self.requestSongData! += self.requestSongDataPage!
                        Pageination.isPageRefreshing = false
                        //self.requestFriend = arrDashboard
                    }
                  
                } catch {
                    print("JSON Serialization error")
                }
                
                break
            case .failure(let error):
                
                print(error.description)
            }
        }
    }
    
}
